package main

import (
	"flag"
	"log"

	"luxor/stratocaster/rpc"
)

func main() {
	host := flag.String("host", "", "Listen address hostname or IP, e.g. [10.0.0.1], Note default will listen on interfaces")
	port := flag.String("port", "8000", "Listen address port number, e.g. [1024..65535], Note, must run as roon if n < 1024")
	flag.Parse()
	opts := rpc.Options{
		Host: *host,
		Port: *port,
	}
	log.Fatalln(rpc.Listen(opts))
}
