package main

import (
	"flag"
	"log"

	"luxor/stratocaster/rpc"
)

func main() {
	host := flag.String("host", "127.0.0.1", "Connect address hostname or IP, e.g. [10.0.0.1]")
	port := flag.String("port", "8000", "Connect address port number, e.g. [1024..65535]")
	flag.Parse()
	opts := rpc.Options{
		Host: *host,
		Port: *port,
	}
	log.Fatalln(rpc.Connect(opts))
}
