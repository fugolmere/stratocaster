README - Stratocaster Stratum v1
================================
Statum Mining Server (`stratosrv`)
Statum Mining Client (`stratocli`)


Info
----
### Summary ###
    # Terminal 1 (i.e. server exe)
    $ go run cmd/stratosrv/main.go [-host=hostname] [-port=portnum]

    # Terminal 2 (i.e. client exe)
    $ go run cmd/stratocli/main.go [-host=hostname] [-port=portnum]


### Description ###
Stratocaster provides a mock server and client of Stratum v1 protocol. The following 3 client methods are implemented:  

1. `mining.configure`
2. `mining.authorize`
3. `mining.subscribe`

Methods 1 and 2 are canned/static requests. Method 3 accepts state (e.g. extranonce1) from the server,  
and maintains it localy for the duration of the client session. Multiple `mining.subscribe` requests will
return the same result until the process is terminated and a new socket connection is made.

Currently, the server only retains state in-memory will not be persisted between invocations.


Code
----
The code consists of the following layers:

1. Service
2. RPC/Controller
3. Storage/Repository

Service layer contains domain/usecase logic.  
RPC/Controller handles user input. It depends on Service to process requests.  
Storage/Repository persists processed data. Service depends on it to store results.


TODO
----
1. Implement persistent storage repository
2. Refactor client/server RPC controller to enable concurrent reads/writes (e.g. `mining.notify`) 
3. Add tests
