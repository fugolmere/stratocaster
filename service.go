package stratocaster

import (
	"fmt"
	"log"
	"strconv"
	"time"
)

type Repository interface {
	PutAuthorize(username string, timestamp time.Time) error
	GetSubscribe(nonce string) (recID int64, err error)
	PutSubscribe() (recID int64, err error)
}

type Service struct {
	repo Repository
}

func NewService(repo Repository) *Service {
	return &Service{
		repo: repo,
	}
}

func (s *Service) Authorize(username, password string) error {
	timestamp := time.Now().UTC()
	return s.repo.PutAuthorize(username, timestamp)
}

func (s *Service) Subscribe(nonce string) (id1, id2, nonce1 string, nonce2Size int, err error) {
	var recID int64
	if recID, err = s.repo.GetSubscribe(nonce); err != nil {
		log.Printf("Warn: failure resuming previous session: %s\n", err)
		if recID, err = s.repo.PutSubscribe(); err != nil {
			log.Printf("Error: failure creating new session: %s\n", err)
			return
		}
		log.Printf("Info: created new session: %d\n", recID)
	}
	id1, id2 = createSubIDs(recID)
	nonce1 = createNonce(recID)
	nonce2Size = 4 // Statically set to 4.
	return
}

func createSubIDs(recID int64) (string, string) {
	id := strconv.FormatInt(recID, 10) // Dec string.
	id = fmt.Sprintf("%08v", id)       // Pad with leading zeros.
	return "luxorminer_" + id, "miner_" + id
}

func createNonce(recID int64) string {
	id := strconv.FormatInt(recID, 16) // Hex string.
	return fmt.Sprintf("%08v", id)     // Pad with leading zeros.
}
