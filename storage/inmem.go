package storage

import (
	"fmt"
	"strconv"
	"sync"
	"time"
)

type InMem struct {
	authorizeMutex sync.Mutex
	subscribeMutex sync.Mutex
	authorizeStore []authorizeRecord
	subscribeStore []subscribeRecord
}

func NewInMem() *InMem {
	return &InMem{
		authorizeStore: make([]authorizeRecord, 0),
		subscribeStore: make([]subscribeRecord, 0),
	}
}

func (m *InMem) GetAuthorize(username string) ([]time.Time, error) {
	timestamps := make([]time.Time, 0)
	for _, v := range m.authorizeStore {
		if v.username == username {
			timestamps = append(timestamps, v.timestamp)
		}
	}
	if len(timestamps) == 0 {
		return nil, fmt.Errorf("no authorize request(s) for user: %s", username)
	}
	return timestamps, nil
}

func (m *InMem) PutAuthorize(username string, timestamp time.Time) error {
	m.authorizeMutex.Lock()
	rec := authorizeRecord{
		username:  username,
		timestamp: timestamp,
	}
	m.authorizeStore = append(m.authorizeStore, rec)
	m.authorizeMutex.Unlock()
	return nil
}

func (m *InMem) GetSubscribe(nonce string) (int64, error) {
	if nonce == "" {
		return 0, fmt.Errorf("empty nonce string")
	}
	id, err := strconv.ParseInt(nonce, 16, 64)
	if err != nil {
		return 0, err
	}
	for _, v := range m.subscribeStore {
		if int64(v) == id {
			return int64(v), nil
		}
	}
	return 0, fmt.Errorf("no subscription found for nonce: %s", nonce)
}

func (m *InMem) PutSubscribe() (int64, error) {
	m.subscribeMutex.Lock()
	id := int64(len(m.subscribeStore) + 1)
	m.subscribeStore = append(m.subscribeStore, subscribeRecord(id))
	m.subscribeMutex.Unlock()
	return id, nil
}

type authorizeRecord struct {
	username  string
	timestamp time.Time
}

type subscribeRecord int64
