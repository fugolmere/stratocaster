package rpc

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"
)

var (
	onceClient sync.Once
	nonce      string
)

func connect(netw, addr string) error {
	log.Printf("Start: connect %s %s\n", netw, addr)
	fd, err := net.Dial(netw, addr)
	if err != nil {
		return err
	}
	defer fd.Close()
	for {
		// Read from user.
		fmt.Println("Select the following:")
		fmt.Println("'1' for mining.configure")
		fmt.Println("'2' for mining.authorize")
		fmt.Println("'3' for mining.subscribe")
		fmt.Print("Send: ")
		msg, err := bufio.NewReader(os.Stdin).ReadString('\n')
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error: failure reading input: %v\n", err)
			continue
		}

		// Create static request.
		msg = strings.TrimSuffix(msg, "\n")
		req, err := createRequest(msg)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error: failure creating request: %v\n", err)
			continue
		}

		// Send request to server.
		if _, err := fd.Write(append(req, '\n')); err != nil {
			fmt.Fprintf(os.Stderr, "Error: failure sending request: %v\n", err)
			continue
		}

		// Recv response from server.
		rsp, err := bufio.NewReader(fd).ReadString('\n')
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error: failure receiving response: %v\n", err)
			if err == io.EOF {
				return err
			}
			continue
		}
		fmt.Print("Recv: " + rsp)

		// Handle response.
		rsp = strings.TrimSuffix(rsp, "\n")
		handleResponse(msg, rsp)
	}
}

func createRequest(msg string) ([]byte, error) {
	num, err := strconv.Atoi(msg)
	if err != nil {
		return nil, fmt.Errorf("failure parsing input: %s", err)
	}
	switch num {
	case 1:
		return createRequestMiningConfigure()
	case 2:
		return createRequestMiningAuthorize()
	case 3:
		return createRequestMiningSubscribe()
	default:
		return nil, fmt.Errorf("failure parsing input: %d not a vaild value", num)
	}
}

func createRequestMiningConfigure() ([]byte, error) {
	// Canned static request params.
	params := []byte(`[["version-rolling"],{"version-rolling.mask":"1fffe000","version-rolling.min-bit-count":3}]`)
	req := Request{
		ID:     1,
		Method: "mining.configure",
		Params: params,
	}
	return json.Marshal(req)
}

func createRequestMiningAuthorize() ([]byte, error) {
	// Canned static request params.
	params := []byte(`["payout_joshf.rigminer_test",""]`)
	req := Request{
		ID:     2,
		Method: "mining.authorize",
		Params: params,
	}
	return json.Marshal(req)
}

func createRequestMiningSubscribe() ([]byte, error) {
	vals := MiningSubscribeParams{
		UserAgent:   "fakeminer/1.0.0",
		ExtraNonce1: nonce,
	}
	params, err := json.Marshal(vals)
	if err != nil {
		return nil, err
	}
	req := Request{
		ID:     3,
		Method: "mining.subscribe",
		Params: params,
	}
	return json.Marshal(req)
}

func handleResponse(msg, rsp string) {
	num, err := strconv.Atoi(msg)
	if err != nil {
		log.Printf("Error: failure handling response: %s\n", err)
		return // Do nothing for now.
	}
	switch num {
	case 3: // Using 3 to indicate 'mining.subscribe' method.
		handleResponseMiningSubscribe([]byte(rsp))
	}
}

func handleResponseMiningSubscribe(msg []byte) {
	var rsp Response
	if err := json.Unmarshal(msg, &rsp); err != nil {
		log.Printf("Error: failure handling response: %s\n", err)
		return
	}
	var rst MiningSubscribeResult
	if err := json.Unmarshal(rsp.Result, &rst); err != nil {
		log.Printf("Error: failure handling response: %s\n", err)
		return
	}
	extraNonce1 := rst.ExtraNonce1
	onceClient.Do(func() {
		nonce = extraNonce1 // Store this once per session.
	})
}
