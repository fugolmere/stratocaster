package rpc

import (
	"encoding/json"
	"fmt"
)

type Request struct {
	ID     int             `json:"id"`
	Method string          `json:"method"`
	Params json.RawMessage `json:"params"`
}

type Response struct {
	ID     int             `json:"id"`
	Error  *Error          `json:"error"`
	Result json.RawMessage `json:"result"`
}

type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Trace   string `json:"trace,omitempty"`
}

func (e *Error) MarshalJSON() ([]byte, error) {
	err := []interface{}{
		e.Code,
		e.Message,
		e.Trace,
	}
	return json.Marshal(err)
}

type MiningSubscribeParams struct {
	UserAgent   string `json:"useragent"`
	ExtraNonce1 string `json:"extranonce1"`
}

func (r MiningSubscribeParams) MarshalJSON() ([]byte, error) {
	res := []interface{}{
		r.UserAgent,
		r.ExtraNonce1,
	}
	return json.Marshal(res)
}

func (r *MiningSubscribeParams) UnmarshalJSON(msg []byte) error {
	res := make([]interface{}, 0, 2)
	if err := json.Unmarshal(msg, &res); err != nil {
		return err
	}
	if len(res) <= 0 {
		return nil
	}
	if len(res) > 0 {
		user, _ := res[0].(string)
		r.UserAgent = user
	}
	if len(res) > 1 {
		nonce, _ := res[1].(string)
		r.ExtraNonce1 = nonce
	}
	return nil

}

type MiningSubscribeResult struct {
	Values          [][]string `json:"values"`
	ExtraNonce1     string     `json:"extranonce1"`
	ExtraNonce2Size int        `json:"extranonce2size"`
}

func (r MiningSubscribeResult) MarshalJSON() ([]byte, error) {
	vals := make([]interface{}, 0, len(r.Values))
	for _, v := range r.Values {
		vals = append(vals, v)
	}
	res := []interface{}{
		vals,
		r.ExtraNonce1,
		r.ExtraNonce2Size,
	}
	return json.Marshal(res)
}

func (r *MiningSubscribeResult) UnmarshalJSON(msg []byte) error {
	res := make([]interface{}, 0)
	if err := json.Unmarshal(msg, &res); err != nil {
		return err
	}
	if len(res) != 3 {
		return fmt.Errorf("MiningSubscribeResult must have 3 elements")
	}
	vals, _ := res[0].([]interface{})
	nonce, _ := res[1].(string)
	size, _ := res[2].(int)
	r.Values = make([][]string, 0, len(vals))
	for _, v := range vals {
		s, _ := v.([]string)
		r.Values = append(r.Values, s)
	}
	r.ExtraNonce1 = nonce
	r.ExtraNonce2Size = size
	return nil
}

type MiningAuthorizeParams struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type MiningAuthorizeResult struct {
	Success bool `json:"success"`
}
