package rpc

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net"
	"strings"
)

func listen(netw, addr string) error {
	log.Printf("Start: listen %s %s\n", netw, addr)
	ln, err := net.Listen(netw, addr)
	if err != nil {
		return err
	}
	defer ln.Close()
	return accept(ln)
}

func accept(ln net.Listener) error {
	for {
		fd, err := ln.Accept()
		if err != nil {
			// No great way to check for this until go 1.16, see https://golang.org/issues/4373.
			if !strings.Contains(err.Error(), "use of closed network connection") {
				log.Printf("Error: failure accepting connection: %v\n", err)
			}
			if netErr, ok := err.(net.Error); ok && !netErr.Temporary() {
				return err
			}
			continue
		}
		go handle(fd)
	}
}

func handle(fd net.Conn) {
	for {
		// Recv request.
		req, err := bufio.NewReader(fd).ReadString('\n')
		if err != nil {
			log.Printf("Error: failure receiving request: %v\n", err)
			if err == io.EOF {
				return
			}
		}
		log.Printf("Recv: %s", req)

		// Create response.
		msg := strings.TrimSuffix(req, "\n")
		rsp, err := createResponse(msg)
		if err != nil {
			log.Printf("Error: failure creating response: %v\n", err)
			continue
		}

		// Send response to client.
		rsp = append(rsp, '\n')
		if _, err := fd.Write(rsp); err != nil {
			log.Printf("Error: failure sending response: %v\n", err)
			continue
		}
		log.Printf("Sent: %s", rsp)
	}
}

func createResponse(msg string) ([]byte, error) {
	// Unmarshall request.
	var req Request
	if err := json.Unmarshal([]byte(msg), &req); err != nil {
		rsp := Response{
			Error: &Error{
				Code:    20,
				Message: "Other/Unknown",
				Trace:   "failure unmarshaling request",
			},
		}
		return json.Marshal(rsp)
	}

	// Route method.
	switch req.Method {
	case "mining.configure":
		return createResponseMiningConfigure(req)
	case "mining.authorize":
		return createResponseMiningAuthorize(req)
	case "mining.subscribe":
		return createResponseMiningSubscribe(req)
	default:
		rsp := Response{
			ID: req.ID,
			Error: &Error{
				Code:    20,
				Message: "Other/Unknown",
				Trace:   fmt.Sprintf("method '%s' not implemented", req.Method),
			},
		}
		return json.Marshal(rsp)
	}
}

func createResponseMiningConfigure(req Request) ([]byte, error) {
	// Canned static response result.
	result := []byte(`{"version.rolling":true, "version-rolling.mask":"1fffe000"}`)
	rsp := Response{
		ID:     req.ID,
		Result: result,
	}
	return json.Marshal(rsp)
}

func createResponseMiningAuthorize(req Request) ([]byte, error) {
	// Canned static response result.
	result := []byte(`true`)
	rsp := Response{
		ID:     req.ID,
		Result: result,
	}
	return json.Marshal(rsp)
}

func createResponseMiningSubscribe(req Request) ([]byte, error) {
	// Get nonce param from request.
	var params MiningSubscribeParams
	if err := json.Unmarshal(req.Params, &params); err != nil {
		log.Printf("Warn: failure reading extranonce1 parameter: %s\n", err)
	}
	nonce := params.ExtraNonce1

	// Subscribe client.
	id1, id2, nonce1, nonce2Size, err := service.Subscribe(nonce)
	if err != nil {
		return nil, err
	}

	// Create result.
	vals := MiningSubscribeResult{
		Values: [][]string{
			[]string{"mining.set_difficulty", id1},
			[]string{"mining.notify", id2},
		},
		ExtraNonce1:     nonce1,
		ExtraNonce2Size: nonce2Size,
	}
	res, err := json.Marshal(vals)
	if err != nil {
		return nil, err
	}

	// Create response.
	rsp := Response{
		ID:     req.ID,
		Result: res,
	}
	return json.Marshal(rsp)
}
