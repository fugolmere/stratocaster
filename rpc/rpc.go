package rpc

import (
	"sync"

	"luxor/stratocaster"
	"luxor/stratocaster/storage"
)

type Service interface {
	Subscribe(nonce string) (id1, id2, nonce1 string, nonce2Size int, err error)
}

var (
	onceServer sync.Once
	service    Service
)

type Options struct {
	Host string
	Port string
}

func Listen(opts Options) error {
	onceServer.Do(func() {
		service = stratocaster.NewService(storage.NewInMem())
	})
	net := "tcp"
	addr := opts.Host + ":" + opts.Port
	return listen(net, addr)
}

func Connect(opts Options) error {
	net := "tcp"
	addr := opts.Host + ":" + opts.Port
	return connect(net, addr)
}
